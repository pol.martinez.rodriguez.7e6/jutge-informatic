package dao

import data.Intents
import data.Preguntas
import data.User
import java.sql.SQLException

class Dao: IDao {
    private val connection = IDao.createConnection()!!

    override fun createUser(user: User): Boolean {
        try {
            val statement = connection.createStatement()
            val array = user.nivellsComplets!!.joinToString(",", "{", "}")
            val sentenceInsert = "INSERT INTO usuari VALUES (DEFAULT, '${user.name}', '${user.lastName}', '${user.password}', '$array', 'alumne')"
            statement.executeUpdate(sentenceInsert)
            return true
        }catch (e:SQLException){
            println("[ERROR] Failed insert create User data | Error Code:${e.errorCode}: ${e.message}")
        }
        return false
    }

    override fun getUserInformationByName(name: String, lastName: String): User? {
        var newUser :User? = null
        try {
            val selectQuery = "SELECT * FROM usuari WHERE nom ='$name' AND cognom = '$lastName'"
            val statement = connection.createStatement()
            val result = statement.executeQuery(selectQuery)
            while (result.next()){
                val id = result.getInt("id_usuari")
                val userName  = result.getString("nom")
                val cognom = result.getString("cognom")
                val contrasenya = result.getString("contrasenya")
                val problemasResolts = (result.getArray("problemesresolts").array as kotlin.Array<*>).filterIsInstance<String>()
                val usuari = result.getString("tipususuari")
                newUser = User( userName, cognom,contrasenya, id, problemasResolts)
            }
            return newUser
        }catch (e:SQLException){
            println("[ERROR] Failed insert create User data | Error Code:${e.errorCode}: ${e.message}")
        }
        return null
    }

    override fun getAllUsers(): List<User>? {
        var users = mutableListOf<User>()
        try {
            val selectQuery = "SELECT * FROM usuari WHERE tipususuari = 'alumne'"
            val statement = connection.createStatement()
            val result = statement.executeQuery(selectQuery)
            while (result.next()){
                val id = result.getInt("id_usuari")
                val userName  = result.getString("nom")
                val cognom = result.getString("cognom")
                val contrasenya = result.getString("contrasenya")
                val problemasResolts = (result.getArray("problemesresolts").array as kotlin.Array<*>).filterIsInstance<String>()
                val usuari = result.getString("tipususuari")
                users.add(User( userName, cognom,contrasenya, id, problemasResolts))
            }
            return users.ifEmpty { null }
        }catch (e:SQLException){
            println("[ERROR] Failed insert create User data | Error Code:${e.errorCode}: ${e.message}")
        }
        return null
    }

    override fun getQuestions(): List<Preguntas> {
        val preguntas = mutableListOf<Preguntas>()
        try {
            val selectQuery = "SELECT * FROM problema "
            val statement = connection.createStatement()
            val result = statement.executeQuery(selectQuery)
            while (result.next()) {
                val id = result.getInt("id_problema")
                val enunciat = result.getString("enunciat")
                val jocDeProvesPub = result.getString("jocdeprovespublic")
                val resultado = result.getString("resultado")
                preguntas.add(Preguntas(id, enunciat, jocDeProvesPub, resultado))
            }
        }catch (e:SQLException){
            println("[ERROR] Failed insert create User data | Error Code:${e.errorCode}: ${e.message}")
        }
        return preguntas
    }

    override fun addQuestion(enunciat: String, jocDeProves: String, result: String): Boolean {
        try {
            val sentenceInsert = "INSERT INTO problema VALUES(DEFAULT, '$enunciat', '$jocDeProves', '$result')"
            val statement = connection.createStatement()
            statement.executeUpdate(sentenceInsert)
            return true
        }catch (e: SQLException){
            println("[ERROR] Failed insert create User data | Error Code:${e.errorCode}: ${e.message}")

        }
        return false
    }

    override fun saveRecord(intents: Intents): Boolean {
        try {
            val sentenceInsert = "INSERT INTO intent VALUES(${intents.idProblem}, ${intents.idAlumne}, ${intents.fails}, '${intents.data}')"
            val statement = connection.createStatement()
            statement.executeUpdate(sentenceInsert)
            return true
        }catch (e:SQLException){
            println("[ERROR] Failed insert create User data | Error Code:${e.errorCode}: ${e.message}")
        }
        return false
    }

    override fun addProblemSolvedToUser(idProblema: Int, user: User) {
        try {
            val statement = connection.createStatement()
            val newList = user.nivellsComplets!!.toMutableList()
            newList.add(idProblema.toString())
            val formattedList = newList.joinToString(",", "{", "}")
            val sentenceUpdate = "UPDATE usuari SET problemesresolts = '$formattedList' WHERE id_usuari = ${user.idAlumne}"
            statement.executeUpdate(sentenceUpdate)
        }catch (e:SQLException){
            println("[ERROR] Failed insert create User data | Error Code:${e.errorCode}: ${e.message}")
        }
    }

    override fun getAllRecords(idAlumne: Int): List<Intents> {
        val intents = mutableListOf<Intents>()
        try {
            val selectQuery = "SELECT * FROM intent WHERE id_usuari = $idAlumne"
            val statement = connection.createStatement()
            val result = statement.executeQuery(selectQuery)
            while (result.next()) {
                val idProblema = result.getInt("id_problema")
                val idUsuari = result.getInt("id_usuari")
                val fails = result.getInt("intents")
                val data = result.getString("dia")
                intents.add(Intents(idProblema, idUsuari, fails, data))
            }
        }catch (e:SQLException){
            println("[ERROR] Failed insert create User data | Error Code:${e.errorCode}: ${e.message}")
        }
        return intents
    }

    override fun addInitialQuestions(problemas: List<Preguntas>) {
        for (i in problemas){
            try {
                val sentenceInsert = "INSERT INTO problema VALUES(DEFAULT, '${i.enunciat}', '${i.jocDeProves}', null)"
                val statement = connection.createStatement()
                statement.executeUpdate(sentenceInsert)
            }catch (e:SQLException){
                println("[ERROR] Failed insert create User data | Error Code:${e.errorCode}: ${e.message}")
            }
        }
    }

    override fun getUserProfessor() : User? {
        var user : User? = null
        try {
            val selectQuery = "SELECT * FROM usuari WHERE tipususuari = 'professor'"
            val statement = connection.createStatement()
            val result = statement.executeQuery(selectQuery)
            while (result.next()){
                val id = result.getInt("id_usuari")
                val userName  = result.getString("nom")
                val cognom = result.getString("cognom")
                val contrasenya = result.getString("contrasenya")
                var problemasResolts :  List<String>? = (result.getArray("problemesresolts").array as kotlin.Array<*>).filterIsInstance<String>()
                if (result.wasNull()) problemasResolts = null
                val usuari = result.getString("tipususuari")
               return User( userName, cognom,contrasenya, id, problemasResolts)
            }
        }catch (e:SQLException){
            println("[ERROR] Failed insert create User data | Error Code:${e.errorCode}: ${e.message}")
        }
        return null

    }


}