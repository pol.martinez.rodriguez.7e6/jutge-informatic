package dao

import data.Intents
import data.Preguntas
import data.User
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

interface IDao {
    fun createUser(user: User):Boolean

    fun getUserInformationByName(name : String, lastName : String): User?

    fun getAllUsers():List<User>?

    fun getQuestions(): List<Preguntas>

    fun addQuestion(enunciat : String, jocDeProves : String, result : String): Boolean

    fun saveRecord(intent: Intents): Boolean

    fun addProblemSolvedToUser(idProblema : Int, user: User)

    fun getAllRecords(idAlumne : Int):List<Intents>

    fun addInitialQuestions (problemas : List<Preguntas>)

    fun getUserProfessor (): User?

    companion object{
        //BD variables
        val jdbcUrl = "jdbc:postgresql://localhost:5432/JutgeITB"
        val username = "postgres"
        val password = "postgres"
        var connection: Connection? = null

        fun createConnection() : Connection? {
            //CONNECTION TO DB
            try {
                connection = DriverManager.getConnection(jdbcUrl, username, password)
                return connection
            } catch (e: SQLException) {
                println("[ERROR] Connection to DB failing | Error Code:${e.errorCode}: ${e.message}")
            }
            return null
        }
    }
}