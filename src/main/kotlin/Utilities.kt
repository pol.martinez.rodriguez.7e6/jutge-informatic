import dao.Dao
import data.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonDecoder
import java.io.EOFException
import java.io.File
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class Utilities {
    val archivoEnunciats = File("src/main/kotlin/data/enunciats.json")
    val formatter = Formatter()
    val dao = Dao()
    fun addInitialQuestions(){
        val listaJsonPreguntas = archivoEnunciats.readLines()
        val preguntas = mutableListOf<Preguntas>()
        for (i in listaJsonPreguntas){
            preguntas.add(Json.decodeFromString(i))
        }
        dao.addInitialQuestions(preguntas)
    }
    fun getAlumneIntents ( user: User) : String {
        val alumneIntents = dao.getAllRecords(user.idAlumne)
        if (alumneIntents.isEmpty()) return "Encara no hi han registres de cap exercici intentat."
        return formatter.formatHistoryForAnUser(alumneIntents.toMutableList(), user)
    }

    fun showAlumnesOnList () : String{
        val alumnes = dao.getAllUsers()!!
        return formatter.stringListOfAlumnes(alumnes)
    }
    fun showAlumneNotas (idAlumne: Int) : String {

        var alumneNotas = MutableList(dao.getQuestions().size){Notas(10,0)}
        val alumneIntents = dao.getAllRecords(idAlumne)
        for (i in alumneIntents){
            if (i.fails != 1) alumneNotas[i.idProblem-1].nota =alumneNotas[i.idProblem-1].nota - i.fails
            if (alumneNotas[i.idProblem-1].nota < 0) alumneNotas[i.idProblem-1].nota = 0
            alumneNotas[i.idProblem-1].tries =+ 1
        }
        for (i in alumneNotas){
            if (i.tries == 0) i.nota = 0
        }
        return formatter.stringNotasAlumne(alumneNotas)
    }
    fun checkAllLevelsComplete (user: User, problemas : List<Preguntas>): Boolean {
        return user.nivellsComplets!!.size == problemas.size
    }

    fun listOfLevels () : String {
        val preguntas = dao.getQuestions()
        if (preguntas.isEmpty()) return "Actualment no hi ha cap nivell."
        return formatter.stringListOfAllLevels(preguntas.toMutableList())
    }
    fun saveRecord (intents: Int, idAlumne: Int, indexPregunta: Int) {
        val format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
        val date = LocalDateTime.now().format(format)
        val intent = Intents(idAlumne,indexPregunta,intents, date)
        dao.saveRecord(intent)
    }
    fun canAddAProblem () : Boolean {
        val jocDeProvesText = File("src/main/kotlin/professor/exercici.txt").readText()
        if (jocDeProvesText == "") return false
        return true
    }

    fun generarFitxerProblemes(index : Int){
        if (dao.getQuestions()[index].result != null) println("El teu document amb els inputs s'ha generat de manera correcte, el seu path és \"src/main/kotlin/results/ex${index + 1}.txt\"")
        when(index){
            0->{
                val file = File("src/main/kotlin/results/ex1.txt")
                file.createNewFile()
                if (file.readText() == ""){
                    for (i in 1.. 300){
                        var firstNumber = 0
                        var secondNumber = 0
                        var thirdNumber = 0
                        for (j in 1 .. 3){
                            when (j) {
                                1-> {
                                    firstNumber = (1..35).random()
                                    file.appendText("\n" + "$firstNumber")
                                }
                                2->{
                                    secondNumber = (2000 .. 100000).random()
                                    file.appendText("\n" + "$secondNumber")
                                }
                                3->{
                                    val rnd = (1..10).random()
                                    if (rnd in (1..5)){
                                        thirdNumber = firstNumber * secondNumber
                                        file.appendText("\n" + "$thirdNumber")
                                    }
                                    else{
                                        if (i%2 == 0){
                                            val minus = (10..304).random()
                                            thirdNumber = firstNumber * secondNumber
                                            thirdNumber -= minus
                                            file.appendText("\n" + "$thirdNumber")
                                        }
                                        else{
                                            val plus = (10..304).random()
                                            thirdNumber = firstNumber * secondNumber
                                            thirdNumber += plus
                                            file.appendText("\n" + "$thirdNumber")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                println("El teu document amb els inputs s'ha generat de manera correcte, el seu path és \"src/main/kotlin/results/ex1.txt\"")
            }
            1->{
                val file = File("src/main/kotlin/results/ex2.txt")
                file.createNewFile()
                if (file.readText() == ""){
                    for (i in 1.. 900){
                        val chars = ('a'..'z')
                        val mayus = ('A'..'Z')
                        val numbers = ('1'..'9')
                        var stringFinal = ""
                        for (j in 1 .. 20){
                            when((1..3).random()){
                                1->{
                                    stringFinal += chars.random()
                                }
                                2->{
                                    stringFinal += mayus.random()
                                }
                                3->{
                                    stringFinal += numbers.random()
                                }
                            }
                        }
                        file.appendText(stringFinal + "\n")
                    }
                }
                println("El teu document amb els inputs s'ha generat de manera correcte, el seu path és \"src/main/kotlin/results/ex2.txt\"")
            }
            2->{
                val file = File("src/main/kotlin/results/ex3.txt")
                file.createNewFile()
                if (file.readText() == ""){
                    val numbers = ('0'..'9')
                    val rnd = ('0'..'9').random()
                    var numberToInsert = 'a'
                    for (i in 1 .. 900){
                        do {
                            numberToInsert = numbers.random()
                        }while (numberToInsert == rnd)
                        file.appendText(numberToInsert.toString())
                    }
                }
                println("El teu document amb els inputs s'ha generat de manera correcte, el seu path és \"src/main/kotlin/results/ex3.txt\"")
            }
            3->{
                val file = File("src/main/kotlin/results/ex4.txt")
                file.createNewFile()
                if (file.readText() == ""){
                    val numeros = mutableListOf(0,1,2,3,4,5,6,7,8,9)
                    numeros.shuffle()
                    var numString = ""
                    for (i in numeros){
                        numString += i.toString()
                    }

                    file.appendText(numString + "\n")
                    for (i in 1..900){
                        val firstValue = (1..9).random()
                        val secondValue = listOf('R','L').random()
                        val thirdValue = (0..9).random()
                        val string = firstValue.toString() + secondValue.toString() + thirdValue.toString()
                        file.appendText(string + "\n")
                    }
                }
                println("El teu document amb els inputs s'ha generat de manera correcte, el seu path és \"src/main/kotlin/results/ex4.txt\"")
            }
            4->{
                val file = File("src/main/kotlin/results/ex5.txt")
                file.createNewFile()
                if (file.readText() == ""){
                    val numeros = (100..200)
                    for (i in 1..900){
                        file.appendText(numeros.random().toString() + "\n")
                    }
                }
                println("El teu document amb els inputs s'ha generat de manera correcte, el seu path és \"src/main/kotlin/results/ex5.txt\"")
            }
            5->{
                val file = File("src/main/kotlin/results/ex6.txt")
                file.createNewFile()
                if (file.readText() == ""){
                    val numeros = (1..10)
                    for (i in 1..200){
                        for (i in 1.. numeros.random()){
                            file.appendText((5..80).random().toString() + "\n")
                        }
                        file.appendText(".\n")
                    }
                }
                println("El teu document amb els inputs s'ha generat de manera correcte, el seu path és \"src/main/kotlin/results/ex6.txt\"")
            }
        }
    }
}