import dao.Dao
import data.*
import java.io.File
import java.io.IOException
import java.util.Scanner

/**
 * The function main is where all the program is executed.
 */
fun main () {
    val scanner = Scanner(System.`in`)
    val utilities = Utilities()
    val dao = Dao()
    lateinit var userInformation : User
    var signInOption = ""
    if (dao.getQuestions().isEmpty()){
        utilities.addInitialQuestions()
    }
    do {
        println("Si has jugat anteriorment i vols continuar jugant amb el teu histoial INCIA SESSIÓ,\n" +
                "sino pots "+"CREAR UN USUARI, introdueix INICIAR o CREAR segons el que volguis fer, o també pots accedir al mode PROFESSOR amb la contrasenya correcte")
        var initiated = false
        var userName = ""
        var lastName = ""
        signInOption = scanner.next().uppercase()
        when(signInOption){
            "INICIAR"-> {
                if (dao.getAllUsers() != null){
                    do {
                        println("Introdueix el teu NOM i COGNOM:")
                        userName = scanner.next().uppercase()
                        lastName = scanner.next().uppercase()
                        if (dao.getUserInformationByName(userName, lastName) == null){
                            println("El usuari introduit no existeix")
                        }
                        else{
                            userInformation = dao.getUserInformationByName(userName, lastName)!!
                        }
                    } while (dao.getUserInformationByName(userName, lastName) == null)
                    println("Introdueix la teva CONTRASENYA.")
                    var password = scanner.next()
                    var fails = 0
                    if (password != userInformation.password) {
                        do {
                            println("La contrasenya introduïda no es la correcta\n")
                            fails += 1
                            if (fails > 2) {
                                println("Si no t'enrecordes pots tornar al menú d'inici, escriu MENU sino esccriu el que sigui.")
                                val restart = scanner.next().uppercase()
                                if (restart == "MENU") break
                                else fails = 0
                            }
                            println("Introdueix la teva CONTRASENYA.")
                            password = scanner.next()
                        }while (password != userInformation.password)
                        if (password == userInformation.password) initiated = true
                    }
                    else initiated = true
                }else{
                    println("No hi han encara usuaris registrats.")
                }
            }
            "CREAR"->{
                do {
                    println("Introdueix el teu NOM i COGNOM:")
                    userName = scanner.next().uppercase()
                    lastName = scanner.next().uppercase()
                    if (dao.getUserInformationByName(userName, lastName) != null){
                        println("Aquest nom i cognom ja està en ús, utilitza el teu segon cognom sino un alias com a nom.")
                    }
                } while (dao.getUserInformationByName(userName, lastName) != null)
                println("Introdueix la teva CONTRASENYA.")
                val password = scanner.next()
                if (dao.createUser(User(userName,lastName,password,0, listOf()))){
                    println("Usuari creat correctament")
                    initiated = true
                    userInformation = dao.getUserInformationByName(userName, lastName)!!
                }
                else{
                    println("Algo ha anat malament torna ho a intentar més tard")
                }
            }
            "PROFESSOR" -> {
                println("""
                    Introdueix la contrasenya del mode professor:
                """.trimIndent())
                val userProfessor = dao.getUserProfessor()!!
                val contrsenyaProfessor = scanner.next().uppercase()
                if (contrsenyaProfessor == userProfessor.password) initiated = true
                else println("Contrasenya errónea")
            }
            else-> println("Introdueix INICIAR, CREAR o PROFESSOR segons el que volguis fer.")
        }
    }while (!initiated)
    val jutge = Jutge()
    var indexOfQuestions = 0
    val preguntas = dao.getQuestions()
    when(signInOption){
        "INICIAR","CREAR"->{
            do {
                showMenu(true)
                val alumneSelection = scanner.next().uppercase()
                var intents = 0
                when (alumneSelection){
                    "1"->{
                        do {
                            if (utilities.checkAllLevelsComplete(userInformation, preguntas)) {
                                println("!! Has completat tots els nivells possibles !!")
                                break
                            }
                            do {
                                if (preguntas[indexOfQuestions].id.toString() in userInformation.nivellsComplets!!) indexOfQuestions +=1
                                if (indexOfQuestions > preguntas.lastIndex) indexOfQuestions = 0
                            } while (preguntas[indexOfQuestions].id.toString() in userInformation.nivellsComplets!!)

                            val pregunta = preguntas[indexOfQuestions]
                            println(preguntas[indexOfQuestions].enunciat +"\n"+ preguntas[indexOfQuestions].jocDeProves)
                            println("""
             T'interessa el problema???   [SI] / [NO] / [BACK]""")
                            val decission = scanner.next().uppercase()
                            when(decission){
                                "SI"->{
                                    intents = 0
                                    utilities.generarFitxerProblemes(indexOfQuestions)
                                    var resolt = false
                                    do {
                                        intents += 1
                                        println("\n Introdueix el teu resultat només amb el numero")
                                        val resposta = scanner.next()
                                        if (jutge.comprovarResultat(indexOfQuestions,resposta)) {
                                            dao.addProblemSolvedToUser(preguntas[indexOfQuestions].id, userInformation)
                                            userInformation.nivellsComplets!!.toMutableList().add(preguntas[indexOfQuestions].id.toString())
                                            utilities.saveRecord(intents,userInformation.idAlumne, preguntas[indexOfQuestions].id)
                                            indexOfQuestions +=1
                                            resolt = true
                                            println("CORRECTE")
                                        }
                                        else{
                                            println("El resultat introduit no és el correcte")
                                        }
                                    }while (!resolt)
                                }
                                "NO"-> indexOfQuestions +=1
                                "BACK"-> break
                                else -> println("""
             T'interessa el problema???   [SI] / [NO]""")
                            }
                        }while (true)
                    }
                    "2"->{
                        println("Els nivells disponibles són els següents: \n" +
                                utilities.listOfLevels() + "\n" +
                            "Escolleix un dels nivells"
                        )
                        var level = scanner.nextInt() - 1
                        while (level !in preguntas.indices){
                            println("Escoll un dels nivells disponibles")
                            level = scanner.nextInt() - 1
                        }
                        println(preguntas[level].enunciat +"\n"+ preguntas[level].jocDeProves)
                        utilities.generarFitxerProblemes(level)
                        intents = 0
                        var resolt = false
                        do {
                            intents +=1
                            println("\n Introdueix el teu resultat només amb el numero")
                            val resposta = scanner.next()
                            if (jutge.comprovarResultat(level,resposta)) {
                                if (preguntas[level].id.toString() !in userInformation.nivellsComplets!!){
                                    dao.addProblemSolvedToUser(preguntas[level].id, userInformation)
                                    userInformation.nivellsComplets!!.toMutableList().add(preguntas[level].id.toString())
                                }
                                utilities.saveRecord(intents,userInformation.idAlumne, preguntas[level].id)
                                resolt = true
                                println("CORRECTE")
                            }
                            else{
                                println("El resultat introduit no és el correcte")
                            }
                        }while (!resolt)
                    }
                    "3"->{
                        println("Aquest és el teu historial: ")
                        println(utilities.getAlumneIntents(userInformation) + "\n")
                    }
                    "4"->{
                        println("""
                            Benvingut a l'apartat d'ajudes del jutge informàtic, aquí t'explicaré de manera breu i entenedora 
                            com funcionen les diferents parts del jutge.
                                1. El mode "Seguir amb l'itinerari" és un mode el qual van sortint exercicis en ordre i tú pots escollir quin fer i quin no seguint un ordre.
                                2. La opció "Llista de problemes" mostra els problemes disponibles en aquest moment i et deixa escollir un per fer.
                                3. Si vols consultar el teu históric de problemes resolts t'apareixerà una llista amb els diferents intents que has anat realitzant,
                                   i un color distinctiu per que et facis una idea de si ha estat prou ben realitzat. (Si estàs en verd o groc vas bé).
                            Els fitxers dels exercicis es generen de manera automàtica per evitar que calculeu la resposta a mà, o que un company us digui la seva resposta,
                            però es possible que alguns exercicis se't doni el joc de proves privat per la terminal, només et cal copiar-lo, el mètode de tractament de les dades 
                            és de lliure elecció.
                        """.trimIndent())
                    }
                    "EXIT"->{}
                    else -> println("Has d'introduir una de les opcions")
                }
            }while (alumneSelection != "EXIT")
        }
        else ->{
            do {
                showMenu(false)
                val professorSelection = scanner.next().uppercase()
                when(professorSelection){
                    "1"->{
                        val jocDeProves = File("src/main/kotlin/professor/exercici.txt")
                        jocDeProves.delete()
                        jocDeProves.createNewFile()
                        println("""
Hey teacher veig que vols afegir algún exercici, t'explico com va aixó:
per afegir un exercici et demanaré un enunciat un joc de proves que serà el que haurá de utiitzar l'alumne per ressoldre el problema
i el resultat d'aquest.
Per fer-ho senzill haurás d'introduir el joc de proves al fitxer que té com a path src/main/kotlin/professor/exercici.txt

                            """.trimMargin())
                        println("Ara introdueix l'enunciat amb el joc de proves públic del nou exercici que vols afegir i quan acabis d'escriure'l escriu END, " + " \n" +
                                "per separar l'enunciat del joc de proves primer introdueix l'enunciat END joc de proves i END:")
                        var word = ""
                        var enunciat = ""
                        var jocDeProvesPrivat = ""
                        for (i in 1..2) {
                            if (i == 1) {
                                while (true) {
                                    word = scanner.next()
                                    if (word == "END") break
                                    enunciat += "$word "
                                }
                            }
                            else{
                                while (true){
                                    word = scanner.next()
                                    if (word == "END") break
                                    jocDeProvesPrivat += "$word "
                                }
                            }
                        }
                        println("Ara introdueix el resultat de l'exercici (Ha de ser només de 1 paraula o grup de números sense espais, ni punts ni comes.):")
                        val exerciciResult = scanner.next()
                        if (utilities.canAddAProblem()){
                            if (dao.addQuestion(enunciat,jocDeProvesPrivat, exerciciResult)) println("S'ha afegit correctament")
                            else println("Hi ha hagut algun problema, torna-ho a intentar més tard")
                        }
                        else {
                            do {
                                println("El fitxer de joc de proves no pot estar buit, has introduit ja el text? [SI]")
                                val textOnJocDeProves = scanner.next()
                                when(textOnJocDeProves){
                                    "SI"->{}
                                    else-> println("Digues SI quan ja ho tinguis")
                                }
                            }while (!utilities.canAddAProblem() && textOnJocDeProves != "SI")
                            if (dao.addQuestion(enunciat,jocDeProvesPrivat, exerciciResult)) println("S'ha afegit correctament")
                            else println("Hi ha hagut algun problema, torna-ho a intentar més tard")
                        }
                    }
                    "2"->{
                        println("Selecciona un dels alumnes registrats al sistema per veure les seves notas \n Alumnes del sistema: \n")
                        println(utilities.showAlumnesOnList())
                        println("Escolleix l'alumne que volguis per l'id:")
                        try {
                            val alumneSelected = scanner.nextInt()
                            if (dao.getAllUsers() != null){
                                if (alumneSelected !in (1..dao.getAllUsers()!!.size)) println("introdueix un dels usuaris del sistema")
                                else println(utilities.showAlumneNotas(alumneSelected))
                            }else{
                                println("Encara no hi han alumnes al sistema ")
                            }
                        }catch (e: IOException){
                            println("introduiex un número vàlid")
                        }
                    }
                    "EXIT"->{}
                    else ->{println("Introdueix una de les opcions del menú")}
                }
            }while (professorSelection != "EXIT")
        }
    }
}
private fun showMenu (alumne : Boolean) {
    if (alumne){
        println(
            """
            1) Seguir amb l'itinerari d'aprenentatge
            2) Llista de problemes
            3) Consultar històric de problemes resolts
            4) Ajuda
            EXIT
        """.trimIndent()
        )
    }
    else {
        println(
            """
            1) Afegir problemes
            2) Treure report de la feina de l'alumne
            EXIT
        """.trimIndent()
        )
    }
}