package data

import kotlinx.serialization.Serializable

@Serializable
data class User(
    val name : String,
    val lastName : String,
    val password : String,
    val idAlumne : Int,
    var nivellsComplets : List<String>?
    )