package data

import kotlinx.serialization.Serializable

@Serializable
data class Preguntas (
    val id : Int,
    val enunciat : String,
    val jocDeProves : String,
    val result : String?
)