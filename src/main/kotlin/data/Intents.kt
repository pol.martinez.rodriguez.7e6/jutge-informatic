package data

import kotlinx.serialization.Serializable

@Serializable
data class Intents (
    val idProblem : Int,
    val idAlumne : Int,
    val fails : Int,
    val data : String
)