import data.*
import java.io.IOException

class Formatter {
    val backgroundRed = "\u001b[41m"
    val backgroundYellow = "\u001b[43m"
    val backgroundGreen = "\u001b[42m"
    val reset = "\u001b[0m"

   fun formatHistoryForAnUser (intents : MutableList<Intents>, alumne : User): String {
       var colorFails = ""
       var indice = 1
       var totalIntentsString = ""
       for (i in intents){
           if (i.fails in (1..2)) colorFails = backgroundGreen
           else if (i.fails in (3..5)) colorFails = backgroundYellow
           else colorFails = backgroundRed
           val stringIntent = """
               Alumne: ${alumne.name} ${alumne.lastName} 
               Exercici: ${i.idProblem}
               Dia: ${i.data.split(" ")[0]}  Hora: ${i.data.split(" ")[1]}
               Errors: ${i.fails} $colorFails   $reset
           """.trimIndent()
           indice +=1
           totalIntentsString += stringIntent + "\n\n"
       }
       return totalIntentsString
   }
    fun stringListOfAllLevels (preguntas : MutableList<Preguntas>) : String{
        var stringOfLevels = ""
        var row = 0
        for (i in preguntas.indices){
            if (row == 4) {
                stringOfLevels += "\n"
                row = 0
            }
            stringOfLevels += " ${i+1} "
            row +=1
        }
        return stringOfLevels
    }
    fun stringListOfAlumnes (alumnes : List<User>) : String {
        var stringListAlumnes = ""
        for (i in alumnes){
            val alumneString = """
               Alumne: ${i.name} ${i.lastName}  idAlumne : ${i.idAlumne}
           """.trimIndent()
            stringListAlumnes += alumneString + "\n\n"
        }
        return stringListAlumnes
    }
    fun stringNotasAlumne (notas: MutableList<Notas>) : String {
        var stringNotas = ""
        var colorFails = ""
        for (i in notas.indices){
            if (notas[i].nota in (10 downTo 9)) colorFails = backgroundGreen
            else if (notas[i].nota in (8 downTo 5)) colorFails = backgroundYellow
            else colorFails = backgroundRed
            val notaExercici = """
        $colorFails                                         $reset
        $colorFails   $reset Exercici: ${i+1}         Intents: ${notas[i].tries}   $colorFails    $reset
        $colorFails   $reset                Nota : ${notas[i].nota}          $colorFails    $reset
        $colorFails                                         $reset
           """.trimIndent()
            stringNotas += notaExercici + "\n\n"
        }
        return stringNotas
    }
}