import dao.Dao
import data.Preguntas
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File

class Jutge {
    val utilities = Utilities()
    val dao = Dao()
    fun comprovarResultat(indexPreguntas: Int, userRespuesta: String): Boolean{
        if (dao.getQuestions()[indexPreguntas].result != null) return userRespuesta.toString() == dao.getQuestions()[indexPreguntas].result
        when (indexPreguntas){
            0-> {
                val fileText = File("src/main/kotlin/results/ex1.txt").readLines()
                var firstNumber = 0
                var secondNumber = 0
                var thirdNumber = 0
                var finalNumber = 0
                for (i in fileText.indices){
                    if (fileText[i] != ""){
                        if (i%3 == 1 ){
                            firstNumber = fileText[i].toInt()
                        }
                        else if (i%3 == 2){
                            secondNumber = fileText[i].toInt()
                        }
                        else {
                            thirdNumber = fileText[i].toInt()
                            if (thirdNumber >= (secondNumber * firstNumber)) finalNumber += 3
                            else finalNumber -= 5
                        }
                    }
                }
                return finalNumber == userRespuesta.toInt()
            }
            1->{
                val  fileText = File("src/main/kotlin/results/ex2.txt").readLines()
                var finalNumber = 0L
                for (i in fileText){
                    var number = ""
                    for (j in i){
                        if (j in '1'..'9') number += j
                    }
                    if (number != "") finalNumber += number.toLong()
                }
                return finalNumber == userRespuesta.toLong()
            }
            2->{
                val fileText = File("src/main/kotlin/results/ex3.txt").readText()
                val numbers = mutableListOf<Int>()
                var numberExpected = 0
                for (i in fileText){
                    numbers.add(i.toString().toInt())
                }
                numbers.sort()
                if (numbers[0] == 1)numberExpected = 0
                else if (numbers[numbers.lastIndex] == 8) numberExpected = 9
                else{
                    for (i in numbers.indices-1){
                        if (numbers[i] != numbers[i+1] && numbers[i]+1 != numbers[i+1]){
                            numberExpected = numbers[i]+1
                            break
                        }
                    }
                }
                return  numberExpected == userRespuesta.toInt()
            }
            3->{
                val fileText = File("src/main/kotlin/results/ex4.txt").readLines()
                var secuenciaInicial = mutableListOf<Int>()
                var secuenciaFinal = mutableListOf<Int>()
                for (i in fileText[0]){
                    secuenciaInicial.add(i.toString().toInt())
                }
                for (i in 1 .. fileText.lastIndex){
                    val inicialPossition = fileText[i][0].toString().toInt()
                    val movementDirection = fileText[i][1].toString()
                    val numberOfMovements = fileText[i][2].toString().toInt()
                    secuenciaFinal = secuenciaInicial
                    if (movementDirection == "R"){
                        val endPosition = (inicialPossition-1+numberOfMovements)%10
                        val numberSave = secuenciaInicial[endPosition]
                        secuenciaFinal[endPosition] = secuenciaInicial[inicialPossition-1]
                        secuenciaFinal[inicialPossition-1] = numberSave
                    }
                    else if (movementDirection == "L"){
                        var endPosition = 0
                        if ((inicialPossition-1-numberOfMovements) < 0) {
                            endPosition = inicialPossition-1-numberOfMovements+10
                        }
                        else {
                           endPosition = (inicialPossition-1-numberOfMovements)%10
                        }
                        val numberSave = secuenciaInicial[endPosition]
                        secuenciaFinal[endPosition] = secuenciaInicial[inicialPossition-1]
                        secuenciaFinal[inicialPossition-1] = numberSave
                    }
                    secuenciaInicial = secuenciaFinal
                }
                var buildNumber = ""
                for (i in secuenciaFinal){
                    buildNumber += i.toString()
                }
                return buildNumber == userRespuesta
            }
            4->{
                val fileText = File("src/main/kotlin/results/ex5.txt").readLines()
                var finalNumber = 0
                for (i in 0 until fileText.lastIndex){
                    val firstNumber = fileText[i].toInt()
                    val secondNumber = fileText[i+1].toInt()
                    if (firstNumber < secondNumber) finalNumber ++
                }
                return finalNumber == userRespuesta.toInt()
            }
            5->{
                val fileText = File("src/main/kotlin/results/ex5.txt").readLines()
                var totalDeGastos = 0
                var tienda = mutableListOf<Int>()
                for (i in 0 .. fileText.lastIndex){
                    if (fileText[i] != "." && fileText[i] != "") tienda.add(i.toInt())
                    else if(fileText[i] == "."){
                        var sumaDeLaTienda = 0
                        for (j in tienda){
                            sumaDeLaTienda =+ j
                        }
                        if (sumaDeLaTienda > 250) totalDeGastos =+1
                    }
                }
                return totalDeGastos.toString() == userRespuesta
            }
        }
        return false
    }
}