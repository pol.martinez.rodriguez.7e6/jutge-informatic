
# Jutge Informàtic
## Què és?
El jutge informàtic és un programa creat per practicar ambits bàsics o no tant bàsics de la programació, on pots anar al ritme que tú volguis, ja que inclou metode d'aprenentatge seguint un itinerari  
, però també inclou l'opció d'escollir tu mateix el número del problema que vols realitzar.
Pels professors que vagin a fer la correcció ve implementat un metode el cual recull el que han fet els alumnes i et treu un informe de l'alumne que volguis.

## Com instalar
Primer de tot comencem per clonar el repositori
- `git clone https://gitlab.com/pol.martinez.rodriguez.7e6/jutge-informatic`

Abans d'executar el programa per utilitzar-lo hem de tenir algunes coses presents, com haver-hi instalat previament postgresql al nostre ordinador si és que no el teniem  
en cas que no sapigues com es fa:
- Windows : https://tecnoloco.istocks.club/una-guia-paso-a-paso-para-instalar-postgresql-en-windows/2021-08-11/
- Linux : https://mrhowtos.com/es/como-instalar-postgresql-linux-ubuntu/

Una vegada tinguem el postgres instalat haurem de crear la base de dades, si no saps fer-ho t'explico pas a pas:
- Windows: 
  - 1r. Obrir pgAdmin
  - 2n. Click dret al deplegable amb el nom de postgresql que apareixera a l'esquerra
  - 3r. Escull CREATE DATABASE
  - 4r. Utilitza el nom de JutgeITB per la base de dades
  - 5è. Amb la base de dades seleccionada a l'esquerra prems Alt + Shift + Q se t'obrirà la query tool on pots executar sentencias a la base de dades.
  - 6è. Una vegada se t'obri el query tool busca el botó d'obrir un arxiu i buscas l'arxiu SCRIPT.sql que està al repositori.
  - 7è. Executas la query tool fent F5 o donant-li al botó d'executar.
---
- Linux: 
  - 1r. Obre la terminal i executa aquesta comanda $ sudo su – postgres serveix per accedir al postgres
  - 2n. Executas aquesta comanda CREATE DATABASE "JutgeITB"
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Spain.1252'
    LC_CTYPE = 'Spanish_Spain.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;
  - 3r. Obres l'arxiu SCRIPT.sql que es troba al repositori i vas exxecutant cada bloc a la terminal, copiant i enganxant o copiant tot el text i executant-lo de cop.

## Com executar el programa
Una vegada tinguis el proyecte obert dins el IDE obres l'arxiu Main.kt i l'executes i ja estaria només et queda gaudir.



